# Cloud Consulting Team toolbox
The instructions below are either for Mac OS X or RedHat / CentOS. You can easily figure out instructions for other OS.

## Git client

Install Homebrew with Ruby, then install Git with Homebrew.

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew doctor
brew install git
```

### Reference

[Install Git](https://gist.github.com/derhuerst/1b15ff4652a867391f03#file-mac-md)

## SFTP client

1. Download and install FileZilla client for Mac at [https://filezilla-project.org](https://filezilla-project.org)
1. Define a new site under Site Manager.
1. Once connected, accept the site public key to make it become a known host.

## SSH client

Termius is available in Mac App Store. Once installed, perform some configuration to make it really handy.

### Adding key

1. Select Preferences > Keychain
1. Click “+ ADD KEY”
1. Copy and paste a private key

### Adding identity

1. Select Preferences > Keychain
1. Click “+ ADD IDENTITY”
1. Type user ID and select a key in the keychain

### New group

1. Select Hosts
1. Click “+ NEW GROUP”
1. Select optional values such as Username or Key to be shared by hosts in the group. Groups can also be nested.

### New host

1. Select Hosts
1. Click “+ NEW HOST”
1. Type in address, select ID, key or group as desired.

## IntelliJ IDEA

1. Download [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download/#section=mac).
1. Open the disk image with the default DiskImageMounter. Drag and drop to Application.
1. Disable plugins not in use. Keep plugins such as Git enabled. Install plugins such as Terraform, Ansible, Bash, MarkDown, etc. as needed.
1. Configure and check out from version control right on the welcome window.
1. Import settings from previous version if upgrade.

Note that it is possible to use other IDEs such as Eclipse, Android Studio, Visual Studio etc.. Each IDE is designed to please a particular sub-population. Choose one most handy for your coding needs. IntelliJ seems fit well with cloud implementation.

## Terraform client

1. There is no installation required. The binary can simply be downloaded from their [site](https://www.terraform.io/downloads.html). The binary can be run directly.
1. Make sure that the binary is executable `chmod +x terraform`.
1. Move it to a right location `mv ./terraform /usr/local/bin/terraform`.

## Ansible

The recommended way to install Ansible on Mac OS X is to use pip 9.0.3 or later.

`sudo -H pip install ansible`

## Linux screen sharing - TMUX

Many times it comes with default, In case it not available than

`sudo yum install tmux` | `sudo apt-get install tmux`

Some [tips/tricks with cheatsheet](https://tmuxcheatsheet.com/)

## Yasha

Yasha is a code generator based on Jinja2 template engine. This is required to generate YAML files from Jinja2 template for CPA.

`sudo pip install yasha`

## Missing xcrun after upgrading to Mojave
After upgrading to Mac OS Mojave, `make html` fails with `xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun`.

Update Xcode Command line Tools to fix it.

```
C02XD1G9JGH7:tsi-ccdoc davidyuan$ sudo xcode-select --install
Password:
xcode-select: note: install requested for command line developer tools
```

## Shell utility for json

Almost all modern languages provide libraries for json data. Sometime, it is handy to deal with it in a Unix shell directly. The `jq` command can be installed on Mac OS. 

```brew install jq```

## Virtual environment

It is generally a good habit and sometimes useful to use virtual environment for each project.

```
pip3 install virtualenv
cd my-project/
virtualenv venv
source venv/bin/activate
```

Note that `virtualenv venv --system-site-packages` inherits globally installed packages.

## Find command for command line search

```bash
find . -type f -print | xargs grep "key_words"
```

## Slack and GoogleDocs
Copy and paste a link to a document on GoogleDocs. Answer yes to OAuth questions. Note that you do not have to repeat this in additional workspaces.
