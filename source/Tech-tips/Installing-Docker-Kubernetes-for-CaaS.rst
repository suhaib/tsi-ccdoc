Installing Docker CLI client and Kubernetes client for CaaS
===========================================================

To access UCP in Docker EE via  Kubernetes CLI, Kubernetes CLI client needs to be installed, together with UCP client bundle. If Docker Swarm only, Docker CLI client and the UCP client bundle are required.

UCP client bundle
-----------------

* Client certificate bundle

  * Click `New Client Bundle` -> `Generate Client Bundle` at `Client Bundles <https://hx-ucp.caas.ebi.ac.uk/manage/profile/clientbundle/>`_.
  * Download and unzip the archive.

The bundle assumes the certificates are under the current working directory. Always change directory to where env.sh is to run `source ./env.sh`. You should see something similar to the following::

  Cluster "ucp_hh-ucp.caas.ebi.ac.uk:6443_davidyuan" set.
  User "ucp_hh-ucp.caas.ebi.ac.uk:6443_davidyuan" set.
  Context "ucp_hh-ucp.caas.ebi.ac.uk:6443_davidyuan" modified.

Docker CLI client
-----------------

* Docker CLI client

  * Click `Download MacOS client binary <https://hx-ucp.caas.ebi.ac.uk/macos/docker>`_ and save the binary file `docker`.
  * Make the Docker CLI executable by `chmod +x ./docker`.
  * Move it to binary path via `mv ./docker /usr/local/bin/docker`.

Ensure that the Docker CLI client is connected to UCP by running `docker version` to see UCP version is returned::

  docker version --format '{{.Server.Version}}'
  ucp/3.1.1

Kubernetes CLI client
---------------------

* Kubernetes CLI client

  * Take note of the Kubernetes version at `About Docker Enterprise <https://hx-ucp.caas.ebi.ac.uk/manage/about/kubernetes>`_.
  * Run the following script to download the binary for MacOS.

Make sure to use the correct value for k8sversion::

    # Set the Kubernetes version as found in the UCP Dashboard or API
    k8sversion=v1.11.5

    # Get the kubectl binary.
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$k8sversion/bin/darwin/amd64/kubectl

    # Make the kubectl binary executable.
    chmod +x ./kubectl

    # Move the kubectl executable to /usr/local/bin.
    sudo mv ./kubectl /usr/local/bin/kubectl

Ensure that the Kubernetes CLI client is connected to UCP by running the following command to see the current context::

  kubectl config current-context
  ucp_hh-ucp.caas.ebi.ac.uk:6443_davidyuan

Helm and  Tiller
----------------
On MacOS, user brew::

  brew install kubernetes-helm

Configure Tiller with Kubernetes cluster::

  kubectl create serviceaccount tiller --namespace tsi-test
  kubectl apply -f tiller-role-binding-namespace-admin.yaml
  helm init --upgrade --service-account tiller --tiller-namespace tsi-test

Note that only namespace-admin can be assigned::

  kind: RoleBinding
  apiVersion: rbac.authorization.k8s.io/v1
  metadata:
    name: tiller-binding
    namespace: tsi-test
  subjects:
    - kind: ServiceAccount
      name: tiller
      namespace: tsi-test
  roleRef:
    kind: ClusterRole
    name: namespace-admin
    apiGroup: rbac.authorization.k8s.io

Minio
-----
Install Minio distributed with local Tiller::

  tiller&
  helm install --host localhost:44134 --tiller-namespace tsi-test --set mode=distributed,replicas=4 stable/minio

If the chart runs, The result should look like the following::

  [tiller] 2018/12/21 08:45:25 preparing install for
  [storage] 2018/12/21 08:45:25 getting release "lucky-garfish.v1"
  [tiller] 2018/12/21 08:45:25 info: Created new release name lucky-garfish
  [tiller] 2018/12/21 08:45:25 rendering minio chart using values
  2018/12/21 08:45:25 info: manifest "minio/templates/pvc.yaml" is empty. Skipping.
  2018/12/21 08:45:25 info: manifest "minio/templates/post-install-create-bucket-job.yaml" is empty. Skipping.
  2018/12/21 08:45:25 info: manifest "minio/templates/networkpolicy.yaml" is empty. Skipping.
  2018/12/21 08:45:25 info: manifest "minio/templates/ingress.yaml" is empty. Skipping.
  2018/12/21 08:45:25 info: manifest "minio/templates/deployment.yaml" is empty. Skipping.
  [tiller] 2018/12/21 08:45:25 performing install for
  [tiller] 2018/12/21 08:45:25 executing 0 crd-install hooks for lucky-garfish
  [tiller] 2018/12/21 08:45:25 hooks complete for crd-install lucky-garfish
  [tiller] 2018/12/21 08:45:26 executing 0 pre-install hooks for lucky-garfish
  [tiller] 2018/12/21 08:45:26 hooks complete for pre-install lucky-garfish
  [storage] 2018/12/21 08:45:26 getting release history for ""
  [storage] 2018/12/21 08:45:26 creating release "lucky-garfish.v1"
  [kube] 2018/12/21 08:45:26 building resources from manifest
  [kube] 2018/12/21 08:45:26 creating 4 resource(s)
  [tiller] 2018/12/21 08:45:26 executing 0 post-install hooks for lucky-garfish
  [tiller] 2018/12/21 08:45:26 hooks complete for post-install lucky-garfish
  [storage] 2018/12/21 08:45:26 updating release "lucky-garfish.v1"
  NAME:   lucky-garfish
  [storage] 2018/12/21 08:45:26 getting last revision of "lucky-garfish"
  [storage] 2018/12/21 08:45:26 getting release history for "lucky-garfish"
  [kube] 2018/12/21 08:45:26 Doing get for Secret: "lucky-garfish"
  [kube] 2018/12/21 08:45:26 get relation pod of object: default/Secret/lucky-garfish
  [kube] 2018/12/21 08:45:26 Doing get for ConfigMap: "lucky-garfish"
  [kube] 2018/12/21 08:45:26 get relation pod of object: default/ConfigMap/lucky-garfish
  [kube] 2018/12/21 08:45:26 Doing get for Service: "lucky-garfish"
  [kube] 2018/12/21 08:45:26 get relation pod of object: default/Service/lucky-garfish
  [kube] 2018/12/21 08:45:26 Doing get for StatefulSet: "lucky-garfish"
  [kube] 2018/12/21 08:45:26 get relation pod of object: default/StatefulSet/lucky-garfish
  LAST DEPLOYED: Fri Dec 21 08:45:25 2018
  NAMESPACE: default
  STATUS: DEPLOYED

  RESOURCES:
  ==> v1/Service
  NAME           TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)   AGE
  lucky-garfish  ClusterIP  None        <none>       9000/TCP  0s

  ==> v1beta1/StatefulSet
  NAME           DESIRED  CURRENT  AGE
  lucky-garfish  4        0        0s

  ==> v1/Pod(related)
  NAME             READY  STATUS   RESTARTS  AGE
  lucky-garfish-0  0/1    Pending  0         0s

  ==> v1/Secret
  NAME           TYPE    DATA  AGE
  lucky-garfish  Opaque  2     0s

  ==> v1/ConfigMap
  NAME           DATA  AGE
  lucky-garfish  2     0s


  NOTES:

  Minio can be accessed via port 9000 on the following DNS name from within your cluster:
  lucky-garfish.default.svc.cluster.local

  To access Minio from localhost, run the below commands:

    1. export POD_NAME=$(kubectl get pods --namespace default -l "release=lucky-garfish" -o jsonpath="{.items[0].metadata.name}")

    2. kubectl port-forward $POD_NAME 9000 --namespace default

  Read more about port forwarding here: http://kubernetes.io/docs/user-guide/kubectl/kubectl_port-forward/

  You can now access Minio server on http://localhost:9000. Follow the below steps to connect to Minio server with mc client:

    1. Download the Minio mc client - https://docs.minio.io/docs/minio-client-quickstart-guide

    2. mc config host add lucky-garfish-local http://localhost:9000 AKIAIOSFODNN7EXAMPLE wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY S3v4

    3. mc ls lucky-garfish-local

  Alternately, you can use your browser or the Minio SDK to access the server - https://docs.minio.io/categories/17

Reference
---------
#. `Docker CLI client and access pack <https://docs.docker.com/ee/ucp/user-access/cli/>`_
#. `Kubernetes CLI <https://docs.docker.com/ee/ucp/user-access/kubectl/>`_
#. `CaaS User Guide <https://www.ebi.ac.uk/seqdb/confluence/display/CAAS/User+Guide>`_