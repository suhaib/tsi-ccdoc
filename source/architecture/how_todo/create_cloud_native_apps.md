## How to create cloud native application

### Important rules with developing cloud native application

1. Lightweight containers
2. Developed with best-of-breed languages and frameworks
3. Designed as loosely coupled microservices 
4. Architect with clean separation of stateless and stateful services
5. Independent of OS and server
6. Deployed on self-service, elastic in nature
7. Manage through DevOps processes
8. Automated Capabilities
9. Define, Policy-driven resource allocations

