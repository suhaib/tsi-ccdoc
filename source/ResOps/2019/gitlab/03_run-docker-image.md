## Exercise 3: Download and run the docker image from your gitlab project ##

### Objective ###

Learn how to download and run docker images built from your CI/CD pipelines.

### Introduction ###

If your pipeline ran to completion in the previous exercise, you can now go to the **Registry** tab on your project page and look near the top-left of the page, where you will see a link with your gitlab username and the name of the project in it. Click on that, and you'll see the list of images you've built so far. There will be only one image, tagged **latest**, with a size of about 45 MB:

![](/static/images/resops2019/gitlab-03-docker-registry.png)

You can run that image by first logging into the docker registry for this gitlab instance, giving your username and password when prompted:

```docker login dockerhub.ebi.ac.uk```

then simply run the image - note that you'll have to specify your own username and the name of your project, of course:

```
> docker run dockerhub.ebi.ac.uk/wildish/test
Unable to find image 'dockerhub.ebi.ac.uk/wildish/test:latest' locally
latest: Pulling from wildish/test
7413c47ba209: Pull complete 
0fe7e7cbb2e8: Pull complete 
1d425c982345: Pull complete 
344da5c95cec: Pull complete 
7e8d808bd962: Pull complete 
1b5e91f7df4f: Pull complete 
Digest: sha256:a1fc0a6209ac87cc8a009e8e2bee2cbfb528f246e2fd174d15a741efe7433bf6
Status: Downloaded newer image for dockerhub.ebi.ac.uk/wildish/test:latest
Hello World
Compiled on Tue Jul 30 09:54:56 UTC 2019
```

Docker supports multiple levels of namespacing within a repository, so it's possible to build more than one image from a given code base if you want to. Check the instructions on the Registry page for how to do that. This can be useful if you need it, but you should always consider if it's better to have multiple separate projects instead of a single project that's building many containers.

Now that you're logged into the registry you can even push images there by hand, just like on dockerhub. That, of course, rather defeats the point of having an automatic CI/CD pipeline to do it for you!

### Deploy tokens ###

If you want to run this docker image in a batch script, or a web service, you won't want to have to log into the registry on every machine you use, or every time it gets rebooted. Instead, if you only want to _use_ the images, but not to _upload_ them from the coomand line, you want a way to have read-only access to the registry. Gitlab provides **deploy-tokens** for that purpose, follow the link on the Registry page to learn more about them.

### Conclusion ###

You now know how to inspect the docker registry associated with your gitlab account, how to log into it from the command line, and how to download and run images from it. By using a deploy-token, you can now use them in your pipelines.

### Best Practices ###

- Use the namespace hierarchy if you need to build multiple docker images from the same code base, use separate projects if the code bases are not related.
- Use deploy tokens when you only need read-only access, for batch jobs and web services.