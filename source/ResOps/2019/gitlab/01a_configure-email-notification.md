## Exercise 1a: (Optional) Configure email notification ##

### Objective ###

Customise your email notification settings

### Introduction ###

You can control the email notifications you get from gitlab in great detail. Here's how to customise them:

- Click on your username or avatar in the top right-hand corner, and select **Settings**
- Click on **Notifications** on the navigation bar on the left, then on the button under **Global notification level**

![](/static/images/resops2019/gitlab-05-email-notifications.png)

- Check any/all boxes that might interest you. I've checked all mine, and will uncheck as/when I decide I don't want to get notifications anymore.

![](/static/images/resops2019/gitlab-05-email-option-panel.png)

Elsewhere on the page you'll see that you can choose at the Group or Project level which set of notification presets you want for that group or project.

That's it, you don't need to save the options, they're saved automatically.