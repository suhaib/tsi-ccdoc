## Exercise 1: Log into gitlab, set up your SSH key ##

### Objective ###

Get your account ready for you to use it.

### Introduction ###

Before you can make use of your gitlab account, you need to upload your SSH key to it. Here's how:

- Click on your username or avatar in the top right-hand corner, and select **Settings**

![](/static/images/resops2019/gitlab-01-settings.png)

- Click on **SSH Keys** on the navigation bar on the left

![](/static/images/resops2019/gitlab-01-ssh-keys.png)

- Follow the instructions on the right to upload your public SSH key. Not your private key, only the public one!

### Conclusion ###

You should now be able to push to your gitlab repositories without having to log in with every commit, exactly as with github and other hosting services.

### Best Practices ###

Ideally you should use a different SSH key for every service (gitlab, github, bitbucket, whatever) that requires you to register with a key. This limits the damage if ever one of those keys is compromised.