## Exercise 2: Create a new project ##

### Objective ###

Create a new project and import the sample code, see the build process it triggers.

### Create the project and upload the code, watch the build fail ###

- In the browser, click on **Projects** -> **Your projects**, from the top-left of the window.
- Click 'New Project', on the top-right
- Give the project a name, e.g. 'gitlab-101' or 'my-test-project'
  - add a description if you like, e.g. a reference to this tutorial
  - set the project visibility to 'internal' or 'public' if you want to share it with others
  - **do not* click the 'Initialize repository with README' button
- Click 'Create Project'

- Do the **git config** steps:
```
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR_USERNAME@ebi.ac.uk"
```

- Create an empty directory, go into it, and create an empty git repository there:

```
mkdir ~/gitlab-test
cd ~/gitlab-test
git init
```

- Add the code to your repository, commit it, and push it to the server:

```
tar xvf ~/tsi-ccdoc/tsi-cc/ResOps/scripts/gitlab/tiny-test.tar
git remote add origin git@gitlab.ebi.ac.uk:YOUR_GITLAB_USERNAME/YOUR_PROJECT_NAME.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Then go to the **CI/CD** -> **Pipelines** tab and watch the progress of your pipeline. Unfortunately it will fail!

### Fix the build and try again ###

The build fails because the code which is uploaded points to my personal project, which only I have access to.

To fix this, edit the **.gitlab-ci.yml** file, change the **APPLICATION** name to the name of your project, and the **REGISTRY_USER** to your gitlab username. Save the file, commit it to git, and push it to the server again.

Go to the **CI/CD** -> **Pipelines** tab again, this time it should succeed.

### Conclusion ###

You now know how to create a project in gitlab, how to upload code to it from an existing area on your computer, and how to configure the basic features of the CI/CD pipeline to refer to this project correctly.

### Best Practices ###

Keep your projects small, it's better to have many small projects than a few large ones with everything bundled in together. It keeps the CI/CD pipeline cleaner and faster, as well as being good coding hygene in general.