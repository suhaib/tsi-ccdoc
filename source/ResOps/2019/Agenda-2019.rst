Agenda for ResOps 2019
======================

Dates & booking information
---------------------------

The dates reserved for 2019 are:

+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
|  Date          | Location        | #places | Registration link                                        | Registration Close  |
+================+=================+=========+==========================================================+=====================+
| 31 Jul 2019    | Garden Room     |    30   |    `31 July Register <http://bit.ly/cc-resops>`_         | 21 Jul 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 21 Aug 2019    | Garden Room     |    30   |    `21 Aug Register <http://bit.ly/cc-resops-aug19>`_    | 11 Aug 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 04 Sep 2019    | Training room 1 |    30   |    `04 Sep Register <http://bit.ly/cc-resops-sep19>`_    | 25 Aug 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 13 Sep 2019    | Garden Room     |    30   |    `13 Sep Register <http://bit.ly/cc-resops-sep19>`_    | 03 Sep 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 02 Oct 2019    | Garden Room     |    30   |    `02 Oct Register <http://bit.ly/cc-resops-oct19>`_    | 22 Sep 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 18 Oct 2019    | Garden Room     |    30   |    `18 Oct Register <http://bit.ly/cc-resops-oct19>`_    | 08 Oct 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+

Bookings are managed via eventbrite. If you're interested in a particular date and it's already full, please add your name to the waiting list, we'll notify you if a place becomes available.

If we don't get enough bookings for a particular date we may cancel it and ask people to re-subscribe for a different session.

If all the courses fill up we will schedule more. We may also increase the number of places available on the course as we gain experience with it, so again, make use of the waiting list.

Course objectives
-----------------

This workshop will provide some background to cloud computing and practical experience in building, deploying and running applications in cloud platforms - Embassy (OpenStack), Google, Amazon and Azure. Using examples drawn from EMBL-EBI’s experiences in the life-sciences, but using tools and technologies that are cross-platform and cross-domain, attendees will come away with a knowledge as to the user-centric hybrid cloud strategy as well as practical aspects of deploying across different clouds and the architectural considerations around porting applications to a cloud.

Prerequisite
------------

* Download Putty from https://www.putty.org/ or you can use any SSH client that you prefer, including dumb terminals.
* Nano is a full-screen editor with hot keys listed on the bottom. It can handle copy and paste better than vi. Users do not have to memorise commands. We will assume it as the default editor in this workshop. Feel free to use your favourite commandline editors.

Schedule and links
------------------

+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| Time  | Duration | Topic                                                                                                              |
+=======+==========+====================================================================================================================+
| 09.00 | 15 min   | Setting up, signing in, and coffee                                                                                 |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 09.15 | 15 min   | `Introduction to ResOps, Day schedule <../../_static/pdf/resops2019/Introduction.pdf>`_                            |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 09.30 | 15 min   | `Cloud 101 <../../_static/pdf/resops2019/Cloud-101_V1.6.pdf>`_                                                     |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 09:45 | 20 min   | `Porting apps into clouds <../../_static/pdf/resops2019/PortingResearchPipelinesintoClouds.pdf>`_                  |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 10:05 | 35 min   | `Docker 101 <../../_static/pdf/resops2019/Docker-101.pdf>`_                                                        |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 10.40 | 10 min   |  Break                                                                                                             |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 10.50 | 40 min   | `Docker Practical <Docker.html>`_                                                                                  |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 11.30 | 30 min   | `Introduction to Gitlab <../../_static/pdf/resops2019/Introduction-to-Gitlab.pdf>`_                                |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 12.00 | 30 min   | `Gitlab Practical <Gitlab.html>`_                                                                                  |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 12.30 | 45 min   | Lunch                                                                                                              |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 13.15 | 30 min   | `Kubernetes 101 <../../_static/pdf/resops2019/Kubernetes-101_v1.11.pdf>`_                                          |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 13:45 | 20 min   | `Kubernetes Demo <Kubernetes-Demo-2019.html>`_                                                                     |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 14.05 | 45 min   | `Kubernetes Practical <Minikube-and-NGINX-Practical-2019.html>`_                                                   |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 14.50 | 15 min   | Break                                                                                                              |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 15.05 | 60 min   | `Advanced Kubernetes Practical <Scaling-up-Kubernetes.html>`_                                                      |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 16.05 | 20 min   | `Advanced Kubernetes Reading <Important-considerations-for-research-pipelines.html>`_                              |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+
| 16:25 | 15 min   | Q&A and Feedback                                                                                                   |
+-------+----------+--------------------------------------------------------------------------------------------------------------------+

.. +-------+----------+--------------------------------------------------------------------------------------------------------------------+
.. | 11.30 | 30 min   | `Case study of porting Rfam into K8S <../../_static/pdf/resops2019/CaseStudyofPortingRfamintoCloud.pdf>`_          |

