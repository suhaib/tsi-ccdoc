# Docker exercises #

There are several separate exercises, you can follow them in sequence or, if you prefer, jump straight to the exercise that interests you. For the most part, they're in the form of annotated command-line sessions. You can copy/paste the commands straight from the screen to your terminal, but you'll have to be aware of things that change - user names, process IDs etc.

The easy way to get access to the example Dockerfiles used in the exercises is to clone this repository from the command line:

```
# Clone this documentation if you haven't already done so
> git clone https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc
> cd tsi-ccdoc
```

The exercises are:
 - [Running Containers](docker/01_running-containers.md)
 - [Running a Biobox Container](docker/02_running-a-biobox-container.md)
 - [Creating a Docker Image](docker/03_creating-a-docker-image.md)
 - [Optimising Docker Images](docker/04_optimising-docker-images.md)
 - [Using the 'builder' pattern to build small images](docker/05_using-the-builder-pattern.md)
 - [Cleaning up](docker/06_cleaning-up.md) - optional, you may need this eventually, but not when you start

For a practical look at real-world containers used for bioinformatics, check out [http://bioboxes.org/](http://bioboxes.org/)

For more information, there are plenty of good tutorials out there. Two good ones are:

- [https://prakhar.me/docker-curriculum/](https://prakhar.me/docker-curriculum/)
- [https://docs.docker.com/engine/tutorials/](https://docs.docker.com/engine/tutorials/)

There's also one which shows how to build an image for MEGAHIT interactively:

- [https://github.com/ngs-docs/2015-nov-docker/blob/master/docker-intro.rst](https://github.com/ngs-docs/2015-nov-docker/blob/master/docker-intro.rst)
