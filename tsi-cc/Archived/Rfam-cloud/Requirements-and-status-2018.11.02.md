## Requirements and status 2018.11.02

Hi David,

This is great! Thank you so much for all the hard work.

So, to keep you updated with my current state, I’ve followed a similar approach, only that I’m using glusterFS with heketi to dynamically provision working space for the users and enable some form of parallelization. 

The solution will only differ in the PV and PVC .yaml descriptions. This way we can measure the performance of the pipeline on different filing systems.

What I’m working on at the moment is trying out client APIs for the pipeline to communicate with the rest of the cluster from within the pod in order to parallelize the processing of the data. What I mean by this is to launch individual pods to search the 100 sequence files concurrently. Processing 1 file by each pod at a time. Kubernetes Jobs (one of the many forms of k8s deployments) resemble the IBM LSF job arrays, so it is an option to easily implement this. We can decide the optimal number of jobs per user as soon as I have the pipeline communicating with the cluster. 

One extra thing I could use your help with, is the creation of new users. 

The way I currently do this is:
1. Create a new user with a user directory created under /home (/home can also be /mnt/nfs/vault_token)

  `sudo useradd -b /home --create-home user_name`

2. Create a password associated with the user we created in step 1

   `sudo passwd user_name`

3. Modify /etc/ssh/sshd_config to enable the user to ssh to the master node without keys
```
                Match User user_name
    		PasswordAuthentication yes
    		KbdInteractiveAuthentication yes
```

4. Restart ssh to apply changes:

  `sudo service ssh restart`

By executing steps 1-4, I can successfully ssh to the master node with the newly created user, but I do not have access to kubectl. We need to figure out the right permissions to enable newly created user accounts to use the k8s cluster. The reason for doing this is to inject kubectl exec user_name-746d966867-vkr5c -it bash in their ~/.bashrc, so that with an ssh to the master or an edge node they access their entry/home pod. We can use the vault tokens instead of the user name to deploy user associated pods. 

Does this make sense? 

I look forward to receiving your feedback on this. As always I’m open to new ideas too. 

Many thanks in advance.

Kind regards,
Ioanna

Ioanna  Kalvari
Software Developer - Rfam
European Bioinformatics Institute (EMBL-EBI)

Tel:	+ 44 (0) 1223 494279
Fax:	+ 44 (0) 1223 494468
Email: ikalvari@ebi.ac.uk


On Oct 31, 2018, at 4:07 PM, David Yuan <davidyuan@ebi.ac.uk> wrote:

Hi Ioanna,

I have been experimenting the nonroot container with K8S APIs since we last discussed on this subject. I am still using the same ECP apps as before. Here is a summary:

* K8S cluster deployed by ECP is running the latest version of Kubernetes with one master and any number of nodes. In my deployment, I asked for 5 nodes.

```
[centos@tsi1539957622607-k8s-master ~]$ kubectl cluster-info
Kubernetes master is running at https://192.168.0.5:6443
KubeDNS is running at https://192.168.0.5:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
[centos@tsi1539957622607-k8s-master ~]$ kubectl version
Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.1", GitCommit:"4ed3216f3ec431b140b1d899130a69fc671678f4", GitTreeState:"clean", BuildDate:"2018-10-05T16:46:06Z", GoVersion:"go1.10.4", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"9", GitVersion:"v1.9.11", GitCommit:"1bfeeb6f212135a22dc787b73e1980e5bccef13d", GitTreeState:"clean", BuildDate:"2018-09-28T21:35:22Z", GoVersion:"go1.9.3", Compiler:"gc", Platform:"linux/amd64"}
[centos@tsi1539957622607-k8s-master ~]$ kubectl get nodes
NAME                          STATUS   ROLES    AGE   VERSION
tsi1539957622607-k8s-master   Ready    master   10d   v1.9.7
tsi1539957622607-k8s-node-1   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-2   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-3   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-4   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-5   Ready    <none>   10d   v1.9.7
```

* The NFS service with persistent volume is consumed by K8S PersistentVolume and PersistentVolumeClaim.

```
centos@tsi1539957622607-k8s-master ~]$ kubectl apply -f nfs-pv.yml
persistentvolume/nfs created
[centos@tsi1539957622607-k8s-master ~]$ kubectl apply -f nfs-pvc.yml
persistentvolumeclaim/nfs created
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pv
NAME   CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM         STORAGECLASS   REASON   AGE
nfs    1Mi        RWX            Retain           Bound    default/nfs                           110s
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pvc
NAME   STATUS   VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
nfs    Bound    nfs      1Mi        RWX                           86s
[centos@tsi1539957622607-k8s-master ~]$ kubectl describe pv
Name:            nfs
Labels:          <none>
Annotations:     kubectl.kubernetes.io/last-applied-configuration:
                   {"apiVersion":"v1","kind":"PersistentVolume","metadata":{"annotations":{},"name":"nfs"},"spec":{"accessModes":["ReadWriteMany"],"capacity"...
                 pv.kubernetes.io/bound-by-controller: yes
Finalizers:      []
StorageClass:    
Status:          Bound
Claim:           default/nfs
Reclaim Policy:  Retain
Access Modes:    RWX
Capacity:        1Mi
Node Affinity:   <none>
Message:         
Source:
    Type:      NFS (an NFS mount that lasts the lifetime of a pod)
    Server:    192.168.0.6
    Path:      /var/nfs
    ReadOnly:  false
Events:        <none>
[centos@tsi1539957622607-k8s-master ~]$ kubectl describe pvc
Name:          nfs
Namespace:     default
StorageClass:  
Status:        Bound
Volume:        nfs
Labels:        <none>
Annotations:   pv.kubernetes.io/bind-completed: yes
               pv.kubernetes.io/bound-by-controller: yes
Finalizers:    []
Capacity:      1Mi
Access Modes:  RWX
Events:        <none>
Mounted By:    rfam-67dd577c76-4r6x6
```

* The deployment of the nonroot container can consume the PVC as persistent storage, which gives the exact same behaviour as Docker. You can create multiple deployments as you like, which will be scheduled to the same or different nodes. Each deployment is uniquely identified by its name (e.g. rfam-67dd577c76-4r6x6). The K8S cluster itself continue running indefinitely. The pod and the deployment stays with or without a user connecting to it interactively till It is deleted by an administrator. This is what we want for multiple curators on computing and storage.

```
[centos@tsi1539957622607-k8s-master ~]$ kubectl apply -f ./K8S.cluster.rfam.yml
deployment.extensions/rfam created
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
NAME                    READY   STATUS    RESTARTS   AGE
rfam-67dd577c76-4r6x6   1/1     Running   0          33s
[centos@tsi1539957622607-k8s-master ~]$ kubectl exec -it rfam-67dd577c76-4r6x6 -- /bin/bash
centos@rfam-67dd577c76-4r6x6:~$ df -h
Filesystem                                                                                          Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-33557033-c0af25dcb3d03add7b06b5487f2fe1a6f14f832f62ca883e8b33b5553df0d7ad   10G  4.5G  5.6G  45% /
tmpfs                                                                                                64M     0   64M   0% /dev
tmpfs                                                                                               920M     0  920M   0% /sys/fs/cgroup
192.168.0.6:/var/nfs                                                                                100G   32M  100G   1% /mnt
/dev/vda1                                                                                            20G   12G  8.8G  57% /vault/mine
shm                                                                                                  64M     0   64M   0% /dev/shm
tmpfs                                                                                               920M   12K  920M   1% /run/secrets/kubernetes.io/serviceaccount
tmpfs                                                                                               920M     0  920M   0% /proc/scsi
tmpfs                                                                                               920M     0  920M   0% /sys/firmware
centos@rfam-67dd577c76-4r6x6:~$ cd /vault/mine
centos@rfam-67dd577c76-4r6x6:/vault/mine$ ls -l
total 0
drwxr-xr-x. 2 root root 24 Oct 30 12:10 davidsvault
centos@rfam-67dd577c76-4r6x6:/vault/mine$ cd /mnt
centos@rfam-67dd577c76-4r6x6:/mnt$ ls -l
total 0
-rw-r--r--. 1 nobody nogroup   0 Oct 30 17:15 centos.uid=1000(centos) gid=1000(centos) groups=1000(centos).Tue Oct 30 17:15:19 UTC 2018.container
-rw-r--r--. 1 nobody nogroup   0 Oct 30 13:38 copy
drwxr-xr-x. 2 nobody nogroup 113 Oct 30 17:16 davidsvault
-rw-r--r--. 1 nobody nogroup   0 Oct 24 15:13 uid=1000(centos) gid=1000(centos) groups=1000(centos).touched
centos@rfam-67dd577c76-4r6x6:~$ ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
centos       1  0.0  0.1  19960  2144 pts/0    Ss+  13:36   0:00 bash
centos       7  0.0  0.1  19972  2280 pts/1    Ss+  13:38   0:00 /bin/bash
centos      16  0.0  0.1  19972  2140 pts/2    Ss   15:38   0:00 /bin/bash
centos      22  0.0  0.0  38380  1652 pts/2    R+   15:38   0:00 ps aux
```

* The container is now deployed in a pod scheduled on one of the 5 nodes scheduled by the master. In this case, it is on node 4. Thus, K8S does not recommend using the local file system. A pod could be moved or restarted on a different node by the scheduler. The pattern to use NFS backed by a persistent volume is recommended in K8S documentation. We are on the right track following the best practices.

```
[centos@tsi1539957622607-k8s-master ~]$ kubectl describe pods
Name:           rfam-67dd577c76-4r6x6
Namespace:      default
Node:           tsi1539957622607-k8s-node-4/192.168.0.8
Start Time:     Wed, 31 Oct 2018 13:36:02 +0000
Labels:         pod-template-hash=2388133732
                run=rfam
Annotations:    <none>
Status:         Running
IP:             10.35.0.1
Controlled By:  ReplicaSet/rfam-67dd577c76
Containers:
  rfam:
    Container ID:   docker://b81c9af5b899285295a7969507ba5a5a294a1f8f0ef9e2531046766fc4f2e9a9
    Image:          ikalvari/rfam-cloud:user-test
    Image ID:       docker-pullable://ikalvari/rfam-cloud@sha256:4cbd6faf599cddf4a34bd274e2c51f733608e0cf19574d79f6ad3cd132943d47
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Wed, 31 Oct 2018 13:36:06 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /mnt from nfs (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-7f7v8 (ro)
      /vault/mine from rfam-vault (rw)
Conditions:
  Type           Status
  Initialized    True 
  Ready          True 
  PodScheduled   True 
Volumes:
  rfam-vault:
    Type:          HostPath (bare host directory volume)
    Path:          /home/centos/
    HostPathType:  Directory
  nfs:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  nfs
    ReadOnly:   false
  default-token-7f7v8:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-7f7v8
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason                 Age    From                                  Message
  ----    ------                 ----   ----                                  -------
  Normal  Scheduled              3m57s  default-scheduler                     Successfully assigned rfam-67dd577c76-4r6x6 to tsi1539957622607-k8s-node-4
  Normal  SuccessfulMountVolume  3m57s  kubelet, tsi1539957622607-k8s-node-4  MountVolume.SetUp succeeded for volume "rfam-vault"
  Normal  SuccessfulMountVolume  3m57s  kubelet, tsi1539957622607-k8s-node-4  MountVolume.SetUp succeeded for volume "default-token-7f7v8"
  Normal  SuccessfulMountVolume  3m56s  kubelet, tsi1539957622607-k8s-node-4  MountVolume.SetUp succeeded for volume "nfs"
  Normal  Pulled                 3m55s  kubelet, tsi1539957622607-k8s-node-4  Container image "ikalvari/rfam-cloud:user-test" already present on machine
  Normal  Created                3m54s  kubelet, tsi1539957622607-k8s-node-4  Created container
  Normal  Started                3m53s  kubelet, tsi1539957622607-k8s-node-4  Started container
[centos@tsi1539957622607-k8s-master ~]$ Last login: Wed Oct 31 13:35:39 2018 from hx-dnat-245.ebi.ac.uk
[centos@tsi1539957622607-k8s-master ~]$ kubectl describe pv
Name:            nfs
Labels:          <none>
Annotations:     kubectl.kubernetes.io/last-applied-configuration:
                   {"apiVersion":"v1","kind":"PersistentVolume","metadata":{"annotations":{},"name":"nfs"},"spec":{"accessModes":["ReadWriteMany"],"capacity"...
                 pv.kubernetes.io/bound-by-controller: yes
Finalizers:      []
StorageClass:    
Status:          Bound
Claim:           default/nfs
Reclaim Policy:  Retain
Access Modes:    RWX
Capacity:        1Mi
Node Affinity:   <none>
Message:         
Source:
    Type:      NFS (an NFS mount that lasts the lifetime of a pod)
    Server:    192.168.0.6
    Path:      /var/nfs
    ReadOnly:  false
Events:        <none>
```

I think that all the requirements outlined in your email on Friday are met by the two ECP apps. Please let me know if there is anything missing.

By the way, Gianni is enhancing some receipts to make them more robust.

Best regards,

David