# Prototype of nfs and k8s cluster

## 2018/10/25

Hi Ioanna,

Thank you for creating the image. I have done experiments with it. The results and conclusions are recorded in the section of “2018/10/24” on https://gitlab.ebi.ac.uk/davidyuan/tsi-ccdoc/wikis/rfam-cloud/rfam-tmp-results.

I would like to summarise the findings for your convenience.

1. The nonroot user centos was created correctly with the same UID, GID and name as the user on the host. It is extremely important to keep the same UID and GID for proper file ownership in the container and on the host.
2. Volumes from both NSF and local disk can be mounted to the container for read and write with any arbitrary path.
   1. NFS volume is accessed as nobody:nogroup in the container, and nfsnobody:nfsnobody on the host.
   2. Local disk such as /home/centos can be read from and written to as centos:centos both inside and outside.
3. As centos in the container is created with password disabled, it is unable to run 'su -‘. In addition, ‘sudo' is unavailable in the container. There is no way for the nonroot user to elevate its privilege, which secures all the processes in the container.
4. To provide secure persistent storage to multiple users, we can introduce a concept of vault token. We can design the token to be a long string beyond human comprehension. There are many algorithms to make such string unique, valid as UNIX path name, and with low probability of collision, such as hash of a public key.
   1. You assign a unique vault token to a new user. Keep a record which token is assigned to who in a table, which can only be viewed by an administrator.
   2. You give user access to Cloud Portal where they can launch K8S cluster for RFAM. This part has been taken care of by SSO for the portal.
   3. The user creates K8S cluster with their unique vault token. If this is the first time that the token is seen by the system, a new empty vault is created by the system. Otherwise, an existing vault is opened where the data stored by the token owner can be read and written again.
   4. When the user exits, the container itself is destroyed to release computing resources. The user data is persisted in the vault.

The implementation for such vault can be very simple. It is just a subdirectory on the persistent volume with NFS facade for example. The implementation detail can be completely hidden from a user using the container.

In your Ansible script, perform the following when launching the container:

```
# Create a new subdirectory if does not exist, and do nothing if exists.
mkdir /p /mnt/nfs/<vault_token>

# Wrap this in a script
docker run -it --mount type=bind,source=/mnt/nfs/<vault_token>,target=/app/pvol ikalvari/rfam-cloud:user-test
```

Neither the user nor your pipeline would know that they are reading or persisting user data in a unique subdirectory in the container. It is the same logical target /app/pvol that they are dealing with. Different users would not be aware of other’s vault although they always use the same UNIX user (e.g. centos).

Best regards,

David

---

Hi Ioanna,

I did some investigation on how to run processes in a container as nonroot. I wrote down what I found with some other tips and tricks on a wiki page https://gitlab.ebi.ac.uk/davidyuan/tsi-ccdoc/wikis/tech-tips/tips-and-tricks-with-docker. Please take a look.

In your particular case, I think that it makes sense to run the pipeline as nonroot. Can you please confirm whether it is what you normally do? If so, can you please use a nonroot user such as centos when you create or rebuild your Docker image? Here is a snippet for build.

```
FROM openjdk:8-jdk
RUN useradd --create-home -s /bin/bash centos
WORKDIR /home/centos
USER centos
```

Please let me know once you have it built. I would like to try it in my environment.

Thanks and regards,

David
## 2018/10/16
Hi Ioanna,

As I am brand new to EBI, I have no experience with kubespray-ebi-portal. I am unable to comment on kubespray-ebi-portal or compare kubespray-ebi-portal with cloud-portal.

We have done some investigation against your requirements / milestones. I think that we have answers to both #1 and #2. In fact, we have prototype working running on cloud-portal now.

1. We have a pattern called “NFS-Server exporting volume”, which creates a persistent volume with NFS facade for read and write. This one can be used as is by you. (Repository: https://cloud-portal.ebi.ac.uk/repository/NFS-Server%20exporting%20volume) & (Deployment: https://cloud-portal.ebi.ac.uk/deployments/TSI1539635628271)
2. We have another pattern “Kubernetes”, which creates a master and any number of worker nodes. I deployed your container and mounted the volume. (Repository: https://cloud-portal.ebi.ac.uk/repository/Kubernetes) & (Deployment https://cloud-portal.ebi.ac.uk/deployments/TSI1539166885300)

Note that I deployed the persistent volume and K8S cluster on two separate cloud providers: extcloud05 and extcloud06. This is to confirm that your pipeline and persistent data can be separated.

Here are the instructions to make the K8S cluster read / write on the volume. You can script them in Ansible with the “Kubernates” pattern as a starting point.

```
# On server replace with /var/nfs 172.17.0.0/24 (rw,async,all_squash) in /etc/exports
[centos@tsi1539635628271-1 ~]$ sudo vi /etc/exports
[centos@tsi1539635628271-1 ~]$ sudo systemctl restart nfs-server

# On client
[centos@tsi1539166885300-k8s-master ~]$ sudo mkdir /mnt/nfs
[centos@tsi1539166885300-k8s-master ~]$ sudo mount -F 193.62.55.26:/var/nfs /mnt/nfs

# Mount the volume when starting the container
[centos@tsi1539166885300-k8s-master ~]$ docker run -i -t --mount type=bind,source=/mnt/nfs,target=/mnt/nfs ikalvari/rfam-cloud

# The volume is writeable from anywhere
root@858198f42b3e:/# df -h
Filesystem                                                                                          Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-71303370-1907526d5a6b50de3d47f0df811f320cd9b156e67164da6782f2ebb9352b1a9c   10G  4.5G  5.6G  45% /
tmpfs                                                                                                64M     0   64M   0% /dev
tmpfs                                                                                               3.9G     0  3.9G   0% /sys/fs/cgroup
193.62.55.26:/var/nfs                                                                               100G   32M  100G   1% /mnt/nfs
/dev/vda1                                                                                            60G   13G   48G  21% /etc/hosts
shm                                                                                                  64M     0   64M   0% /dev/shm
tmpfs                                                                                               3.9G     0  3.9G   0% /proc/scsi
tmpfs                                                                                               3.9G     0  3.9G   0% /sys/firmware
root@858198f42b3e:/# ls -l /mnt/nfs
total 0
-rw-r--r--. 1 nobody nogroup 0 Oct 16 12:26 david.from.k8s-master
-rw-r--r--. 1 root   root    0 Oct 15 20:40 david.was.here
root@858198f42b3e:/# touch /mnt/nfs/david.from.rfam
root@858198f42b3e:/# ls -l /mnt/nfs
total 0
-rw-r--r--. 1 nobody nogroup 0 Oct 16 12:26 david.from.k8s-master
-rw-r--r--. 1 nobody nogroup 0 Oct 16 12:51 david.from.rfam
-rw-r--r--. 1 root   root    0 Oct 15 20:40 david.was.here
root@858198f42b3e:/#
```

Please let us know whether this is what you are looking for to address #1 & #2. If we misunderstood your bullet points, please let us know.

If you are interested, please take a look. If you want to meet to talk about details, please let me know.

Best regards,

David

