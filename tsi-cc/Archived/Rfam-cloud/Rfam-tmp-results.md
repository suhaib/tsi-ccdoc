# Rfam tmp results

## 2018/10/30

### Cleaning old deployment
```
[centos@tsi1539957622607-k8s-master ~]$ kubectl delete deployment rfam
deployment.extensions "rfam" deleted
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
No resources found.
```
### Creating deployment
The deployment is created on one of the five nodes decided by K8S cluster. The hostPath volume are mounted and accessible. This works nicely for root user. Nonroot user centos can only read and write to existing directories accessible to centos on host. New directories created are owned by root:root on the host. Thus, unusable by centos in the container.
```
[centos@tsi1539957622607-k8s-master ~]$ kubectl create -f ./K8S.cluster.rfam.yml
deployment.extensions/rfam created
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
NAME                    READY   STATUS    RESTARTS   AGE
rfam-5b4d8b9777-ppfr5   1/1     Running   0          7s
[centos@tsi1539957622607-k8s-master ~]$ kubectl describe pod
Name:           rfam-5b4d8b9777-ppfr5
Namespace:      default
Node:           tsi1539957622607-k8s-node-1/192.168.0.13
Start Time:     Tue, 30 Oct 2018 14:36:06 +0000
Labels:         pod-template-hash=1608465333
                run=rfam
Annotations:    <none>
Status:         Running
IP:             10.36.0.1
Controlled By:  ReplicaSet/rfam-5b4d8b9777
Containers:
  rfam:
    Container ID:   docker://60eecab78d6702cfd1865932c089dfe988edc4b95785d79e0aa1939300b5dd50
    Image:          ikalvari/rfam-cloud:user-test
    Image ID:       docker-pullable://ikalvari/rfam-cloud@sha256:4cbd6faf599cddf4a34bd274e2c51f733608e0cf19574d79f6ad3cd132943d47
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 30 Oct 2018 14:36:08 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /app/pvol from rfam-pvol (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-7f7v8 (ro)
      /vault/mine from rfam-vault (rw)
Conditions:
  Type           Status
  Initialized    True 
  Ready          True 
  PodScheduled   True 
Volumes:
  rfam-vault:
    Type:          HostPath (bare host directory volume)
    Path:          /home/centos/
    HostPathType:  DirectoryOrCreate
  rfam-pvol:
    Type:          HostPath (bare host directory volume)
    Path:          /mnf/nfs/
    HostPathType:  DirectoryOrCreate
  default-token-7f7v8:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-7f7v8
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason                 Age   From                                  Message
  ----    ------                 ----  ----                                  -------
  Normal  Scheduled              31s   default-scheduler                     Successfully assigned rfam-5b4d8b9777-ppfr5 to tsi1539957622607-k8s-node-1
  Normal  SuccessfulMountVolume  31s   kubelet, tsi1539957622607-k8s-node-1  MountVolume.SetUp succeeded for volume "rfam-vault"
  Normal  SuccessfulMountVolume  31s   kubelet, tsi1539957622607-k8s-node-1  MountVolume.SetUp succeeded for volume "rfam-pvol"
  Normal  SuccessfulMountVolume  31s   kubelet, tsi1539957622607-k8s-node-1  MountVolume.SetUp succeeded for volume "default-token-7f7v8"
  Normal  Pulled                 29s   kubelet, tsi1539957622607-k8s-node-1  Container image "ikalvari/rfam-cloud:user-test" already present on machine
  Normal  Created                29s   kubelet, tsi1539957622607-k8s-node-1  Created container
  Normal  Started                29s   kubelet, tsi1539957622607-k8s-node-1  Started container
```
### NFS volume and NFS volume claim
```
[centos@tsi1539957622607-k8s-master ~]$ kubectl create -f nfs-pv.yml
persistentvolume/nfs created
[centos@tsi1539957622607-k8s-master ~]$ kubectl create -f nfs-pvc.yml
persistentvolumeclaim/nfs created
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pv
NAME   CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM         STORAGECLASS   REASON   AGE
nfs    1Mi        RWX            Retain           Bound    default/nfs                           110s
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pvc
NAME   STATUS   VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
nfs    Bound    nfs      1Mi        RWX                           86s
[centos@tsi1539957622607-k8s-master ~]$ kubectl describe pv nfs
Name:            nfs
Labels:          <none>
Annotations:     pv.kubernetes.io/bound-by-controller: yes
Finalizers:      []
StorageClass:    
Status:          Bound
Claim:           default/nfs
Reclaim Policy:  Retain
Access Modes:    RWX
Capacity:        1Mi
Node Affinity:   <none>
Message:         
Source:
    Type:      NFS (an NFS mount that lasts the lifetime of a pod)
    Server:    192.168.0.6
    Path:      /
    ReadOnly:  false
Events:        <none>
[centos@tsi1539957622607-k8s-master ~]$ kubectl describe pvc nfs
Name:          nfs
Namespace:     default
StorageClass:  
Status:        Bound
Volume:        nfs
Labels:        <none>
Annotations:   pv.kubernetes.io/bind-completed: yes
               pv.kubernetes.io/bound-by-controller: yes
Finalizers:    []
Capacity:      1Mi
Access Modes:  RWX
Events:        <none>
Mounted By:    <none>
```
## 2018/10/29
### K8S cluster is running the latest version
```
[centos@tsi1539957622607-k8s-master ~]$ kubectl cluster-info
Kubernetes master is running at https://192.168.0.5:6443
KubeDNS is running at https://192.168.0.5:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
[centos@tsi1539957622607-k8s-master ~]$ kubectl version
Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.1", GitCommit:"4ed3216f3ec431b140b1d899130a69fc671678f4", GitTreeState:"clean", BuildDate:"2018-10-05T16:46:06Z", GoVersion:"go1.10.4", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"9", GitVersion:"v1.9.11", GitCommit:"1bfeeb6f212135a22dc787b73e1980e5bccef13d", GitTreeState:"clean", BuildDate:"2018-09-28T21:35:22Z", GoVersion:"go1.9.3", Compiler:"gc", Platform:"linux/amd64"}
[centos@tsi1539957622607-k8s-master ~]$ kubectl get nodes
NAME                          STATUS   ROLES    AGE   VERSION
tsi1539957622607-k8s-master   Ready    master   10d   v1.9.7
tsi1539957622607-k8s-node-1   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-2   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-3   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-4   Ready    <none>   10d   v1.9.7
tsi1539957622607-k8s-node-5   Ready    <none>   10d   v1.9.7
```
### Running Docker container as nonroot interactively
```
[centos@tsi1539957622607-k8s-master ~]$ kubectl run rfam --image=ikalvari/rfam-cloud:user-test -i --tty
kubectl run --generator=deployment/apps.v1beta1 is DEPRECATED and will be removed in a future version. Use kubectl create instead.
If you don't see a command prompt, try pressing enter.
centos@rfam-7c4f978bc5-4wq5k:~$ df -h
Filesystem                                                                                          Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-33557033-294c4b12d594bd3e9e758d83d16b7b3efcc617fd3e56f4084ad1643b8ebf90f1   10G  4.5G  5.6G  45% /
tmpfs                                                                                                64M     0   64M   0% /dev
tmpfs                                                                                               920M     0  920M   0% /sys/fs/cgroup
/dev/vda1                                                                                            20G  6.6G   14G  33% /etc/hosts
shm                                                                                                  64M     0   64M   0% /dev/shm
tmpfs                                                                                               920M   12K  920M   1% /run/secrets/kubernetes.io/serviceaccount
tmpfs                                                                                               920M     0  920M   0% /proc/scsi
tmpfs                                                                                               920M     0  920M   0% /sys/firmware
centos@rfam-7c4f978bc5-4wq5k:~$ exit
exit
Session ended, resume using 'kubectl attach rfam-7c4f978bc5-4wq5k -c rfam -i -t' command when the pod is running
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
NAME                    READY   STATUS             RESTARTS   AGE
rfam-7c4f978bc5-4wq5k   0/1     CrashLoopBackOff   1          7m59s
[centos@tsi1539957622607-k8s-master ~]$ kubectl attach rfam-7c4f978bc5-4wq5k -c rfam -i -t
If you don't see a command prompt, try pressing enter.
centos@rfam-7c4f978bc5-4wq5k:~$ exit
exit
Session ended, resume using 'kubectl attach rfam-7c4f978bc5-4wq5k -c rfam -i -t' command when the pod is running
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
NAME                    READY   STATUS             RESTARTS   AGE
rfam-7c4f978bc5-4wq5k   0/1     CrashLoopBackOff   1          7m59s
[centos@tsi1539957622607-k8s-master ~]$ kubectl delete deployment rfam
deployment.extensions "rfam" deleted
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
No resources found.
```

### Creating deployment YAML file

```
# [centos@tsi1539957622607-k8s-master ~]$ kubectl create deployment rfam -o yaml --image=ikalvari/rfam-cloud:user-test > ./rfam.yml

[centos@tsi1539957622607-k8s-master ~]$ kubectl run rfam --image=ikalvari/rfam-cloud:user-test -i --tty
kubectl run --generator=deployment/apps.v1beta1 is DEPRECATED and will be removed in a future version. Use kubectl create instead.
If you don't see a command prompt, try pressing enter.
centos@rfam-7c4f978bc5-jwj6t:~$ exit
exit
Session ended, resume using 'kubectl attach rfam-7c4f978bc5-jwj6t -c rfam -i -t' command when the pod is running
[centos@tsi1539957622607-k8s-master ~]$ kubectl get deployment rfam -o yaml > ./rfam.yaml
[centos@tsi1539957622607-k8s-master ~]$ kubectl delete deployment rfam
deployment.extensions "rfam" deleted
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
No resources found.
[centos@tsi1539957622607-k8s-master ~]$ kubectl create -f ./rfam.yaml
deployment.extensions/rfam created
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
NAME                    READY   STATUS    RESTARTS   AGE
rfam-7c4f978bc5-7nh9h   1/1     Running   0          38s
[centos@tsi1539957622607-k8s-master ~]$ kubectl attach rfam-7c4f978bc5-7nh9h -c rfam -it
If you don't see a command prompt, try pressing enter.
centos@rfam-7c4f978bc5-7nh9h:~$ exit
exit
Session ended, resume using 'kubectl attach rfam-7c4f978bc5-7nh9h -c rfam -i -t' command when the pod is running
```

### attach hostPath

```
[centos@tsi1539957622607-k8s-master ~]$ vi ./rfam.yaml
[centos@tsi1539957622607-k8s-master ~]$ kubectl create -f ./rfam.yaml
deployment.extensions/rfam created
[centos@tsi1539957622607-k8s-master ~]$ kubectl get pods
NAME                    READY   STATUS    RESTARTS   AGE
rfam-77b58f7bc5-vshqt   1/1     Running   0          72s
[centos@tsi1539957622607-k8s-master ~]$ kubectl attach rfam-77b58f7bc5-vshqt -c rfam -it
If you don't see a command prompt, try pressing enter.
centos@rfam-77b58f7bc5-vshqt:~$ df -h
Filesystem                                                                                          Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-33557032-5f2c128fffea9f3c26917a26eda9ae546302de26b0f973d04690da8c2074eeb4   10G  4.5G  5.6G  45% /
tmpfs                                                                                                64M     0   64M   0% /dev
tmpfs                                                                                               920M     0  920M   0% /sys/fs/cgroup
/dev/vda1                                                                                            20G  6.8G   14G  34% /app/pvol
shm                                                                                                  64M     0   64M   0% /dev/shm
tmpfs                                                                                               920M   12K  920M   1% /run/secrets/kubernetes.io/serviceaccount
tmpfs                                                                                               920M     0  920M   0% /proc/scsi
tmpfs                                                                                               920M     0  920M   0% /sys/firmware
```

## 2018/10/24
```
[centos@tsi1539957622607-k8s-master ~]$ docker run -it --mount type=bind,source=/mnt/nfs,target=/app/pvol ikalvari/rfam-cloud:user-test
Unable to find image 'ikalvari/rfam-cloud:user-test' locally
user-test: Pulling from ikalvari/rfam-cloud
bc9ab73e5b14: Pull complete 
193a6306c92a: Pull complete 
e5c3f8c317dc: Pull complete 
a587a86c9dcb: Pull complete 
a4c7ee7ef122: Pull complete 
a7c0dad691e9: Pull complete 
367a6a68b113: Pull complete 
28351dec2f89: Pull complete 
bfa52db486a3: Pull complete 
62055bc595ea: Pull complete 
32c6ca676e76: Pull complete 
508192e30e39: Pull complete 
d49068b88031: Pull complete 
fbbb4202c62f: Pull complete 
5e91e4fdfd3b: Pull complete 
2a2ac28fbdf4: Pull complete 
79aed26e2783: Pull complete 
85b067b0915d: Pull complete 
562a2101bd60: Pull complete 
f45843a71921: Pull complete 
da161ee463ff: Pull complete 
6063d5035793: Pull complete 
313e815b1f64: Pull complete 
12cc0199a7d4: Pull complete 
02a8931fef9c: Pull complete 
6ec29e9c767b: Pull complete 
caf105bd41fd: Pull complete 
d699425f3eae: Pull complete 
e340eeea6f80: Pull complete 
65318ed77a7a: Pull complete 
b591dbc82fe6: Pull complete 
b3de3fed68df: Pull complete 
44cc52138029: Pull complete 
a4fec9aaa68f: Pull complete 
452bf649a4f8: Pull complete 
0a68db8e05ce: Pull complete 
Digest: sha256:4cbd6faf599cddf4a34bd274e2c51f733608e0cf19574d79f6ad3cd132943d47
Status: Downloaded newer image for ikalvari/rfam-cloud:user-test
centos@16d6c2c2f75b:~$ id
uid=1000(centos) gid=1000(centos) groups=1000(centos)
centos@16d6c2c2f75b:~$ df -h
Filesystem                                                                                      Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-1376-c1038937dc8df4e05f83fdd676011f3fa2fff7fd0c357fdceb22b754cfc5558a   10G  4.5G  5.6G  45% /
tmpfs                                                                                            64M     0   64M   0% /dev
tmpfs                                                                                           920M     0  920M   0% /sys/fs/cgroup
192.168.0.6:/var/nfs                                                                            100G   32M  100G   1% /app/pvol
/dev/vda1                                                                                        20G   13G  7.7G  62% /etc/hosts
shm                                                                                              64M     0   64M   0% /dev/shm
tmpfs                                                                                           920M     0  920M   0% /proc/scsi
tmpfs                                                                                           920M     0  920M   0% /sys/firmware
centos@16d6c2c2f75b:~$ pwd
/home/centos
centos@16d6c2c2f75b:~$ touch ./$USER.$(id).touched
centos@16d6c2c2f75b:~$ ls -l
total 0
-rw-r--r--. 1 centos centos 0 Oct 24 14:38 gid=1000(centos)
-rw-r--r--. 1 centos centos 0 Oct 24 14:38 groups=1000(centos).touched
centos@16d6c2c2f75b:~$ echo $USER

centos@16d6c2c2f75b:~$ exit
exit
[centos@tsi1539957622607-k8s-master ~]$ docker run -it --mount type=bind,source=/mnt/nfs,target=/app/pvol ikalvari/rfam-cloud:user-test
centos@7ada682b8c13:~$ pwd
/home/centos
centos@7ada682b8c13:~$ ls -l
total 0
centos@7ada682b8c13:~$ exit
exit
[centos@tsi1539957622607-k8s-master ~]$ pwd
/home/centos
[centos@tsi1539957622607-k8s-master ~]$ ls -ltr
total 8
-rw-rw-r--. 1 centos centos 5062 Oct 19 14:09 weave.yaml
[centos@tsi1539957622607-k8s-master ~]$ 
```

* Changes are OK as long as the container is running. It is not persisted anywhere. They are removed when the container is stopped.

```
[centos@tsi1539957622607-k8s-master ~]$ docker run -it --mount type=bind,source=/mnt/nfs,target=/app/pvol ikalvari/rfam-cloud:user-test
centos@86ed7c2c56f1:~$ touch /app/pvol/"$(id).touched"
centos@86ed7c2c56f1:~$ ls -l /app/pvol/
total 0
-rw-r--r--. 1 nobody nogroup 0 Oct 24 15:13 uid=1000(centos) gid=1000(centos) groups=1000(centos).touched
centos@86ed7c2c56f1:~$ exit
exit
[centos@tsi1539957622607-k8s-master ~]$ ls -l /mnt/nfs/
total 0
-rw-r--r--. 1 nfsnobody nfsnobody 0 Oct 24 15:13 uid=1000(centos) gid=1000(centos) groups=1000(centos).touched
[centos@tsi1539957622607-k8s-master ~]$ docker run -it --mount type=bind,source=/mnt/nfs,target=/app/pvol ikalvari/rfam-cloud:user-test
centos@1c3e60012d57:~$ ls -l /app/pvol/
total 0
-rw-r--r--. 1 nobody nogroup 0 Oct 24 15:13 uid=1000(centos) gid=1000(centos) groups=1000(centos).touched
centos@1c3e60012d57:~$ 
```

* Persistence happens on the mounted volume as `nobody:nogroup` in the container and `nfsnobody:nfsnobody` on the host.
```
[centos@tsi1539957622607-k8s-master ~]$ docker run -it --mount type=bind,source=/home/centos/,target=/app/pvol ikalvari/rfam-cloud:user-test
centos@f717e91239f3:~$ pwd
/home/centos
centos@f717e91239f3:~$ df -h
Filesystem                                                                                      Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-1376-164f0d5b1864fa04f3ba131f979a6dd349214935c6b14d4ee1dde63cdf231e92   10G  4.5G  5.6G  45% /
tmpfs                                                                                            64M     0   64M   0% /dev
tmpfs                                                                                           920M     0  920M   0% /sys/fs/cgroup
/dev/vda1                                                                                        20G   13G  7.7G  62% /app/pvol
shm                                                                                              64M     0   64M   0% /dev/shm
tmpfs                                                                                           920M     0  920M   0% /proc/scsi
tmpfs                                                                                           920M     0  920M   0% /sys/firmware
centos@f717e91239f3:~$ cd /app/pvol/
centos@f717e91239f3:/app/pvol$ ls -l
total 8
-rw-rw-r--. 1 centos centos 5062 Oct 19 14:09 weave.yaml
centos@f717e91239f3:/app/pvol$ touch ./"$(id).touched.from.container"
centos@f717e91239f3:/app/pvol$ ls -l
total 8
-rw-r--r--. 1 centos centos    0 Oct 24 15:22 uid=1000(centos) gid=1000(centos) groups=1000(centos).touched.from.container
-rw-rw-r--. 1 centos centos 5062 Oct 19 14:09 weave.yaml
centos@f717e91239f3:/app/pvol$ exit
exit
[centos@tsi1539957622607-k8s-master ~]$ pwd
/home/centos
[centos@tsi1539957622607-k8s-master ~]$ ls -l
total 8
-rw-r--r--. 1 centos centos    0 Oct 24 15:22 uid=1000(centos) gid=1000(centos) groups=1000(centos).touched.from.container
-rw-rw-r--. 1 centos centos 5062 Oct 19 14:09 weave.yaml
[centos@tsi1539957622607-k8s-master ~]$ 
```

* The hack seems working. /home/centos in the container is a scratch pad. Anything written to it will disappear when the container exists. /home/centos from the host mapped as /app/pvol/ persists anything written to it. As the user name UID and GID are identical in the container and on the host. Neither container nor the host knows the difference.
* We could preallocate or create them at the deployment time /home/centos/user1 ... /home/centos/user99 on the host or similar directory structure on NFS volume. Bind mount /home/centos/userxxx to /app/pvol when starting a container. A user would not know the difference and everyone has their own private storage. The files would be owned by `centos:centos` and `nobody:nogroup` in the container but by `centos:centos` and `nfsnobody:nfsnobody` on the host.

```
[centos@tsi1539957622607-k8s-master ~]$ docker run -it --mount type=bind,source=/home/centos/,target=/app/pvol ikalvari/rfam-cloud:user-test
centos@ee5a290d3256:~$ su -
Password:
su: Authentication failure
centos@ee5a290d3256:~$ sudo su -
bash: sudo: command not found
centos@ee5a290d3256:~$
```

* The `sudo` command is not available in a container. If the user is created with password login disabled, which is the case, it would not be able to `su` to root, either.

## 2018/10/19
```
[centos@tsi1539957622607-k8s-master ~]$ sudo mkdir /mnt/nfs
[centos@tsi1539957622607-k8s-master ~]$ sudo mount -F 192.168.0.6:/var/nfs /mnt/nfs
[centos@tsi1539957622607-k8s-master ~]$ df -h
Filesystem            Size  Used Avail Use% Mounted on
/dev/vda1              20G  2.6G   18G  13% /
devtmpfs              901M     0  901M   0% /dev
tmpfs                 920M     0  920M   0% /dev/shm
tmpfs                 920M   17M  903M   2% /run
tmpfs                 920M     0  920M   0% /sys/fs/cgroup
tmpfs                 184M     0  184M   0% /run/user/1000
192.168.0.6:/var/nfs  100G   32M  100G   1% /mnt/nfs
[centos@tsi1539957622607-k8s-master ~]$ sudo usermod -a -G docker $USER

# After reconnect
[centos@tsi1539957622607-k8s-master ~]$ docker run -it --mount type=bind,source=/mnt/nfs,target=/app/pvol ikalvari/rfam-cloud
root@7557f3d7b555:/# df -h
Filesystem                                                                                      Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-1376-995f9c77cb33639ace4e62638aecab7af0ae35de04329cde40811d3fc381b05d   10G  4.5G  5.6G  45% /
tmpfs                                                                                            64M     0   64M   0% /dev
tmpfs                                                                                           920M     0  920M   0% /sys/fs/cgroup
192.168.0.6:/var/nfs                                                                            100G   32M  100G   1% /app/pvol
/dev/vda1                                                                                        20G  7.6G   13G  38% /etc/hosts
shm                                                                                              64M     0   64M   0% /dev/shm
tmpfs                                                                                           920M     0  920M   0% /proc/scsi
tmpfs                                                                                           920M     0  920M   0% /sys/firmware
root@7557f3d7b555:/#
```

## 2018/10/16

```
# On server replace with /var/nfs 172.17.0.0/24 (rw,async,all_squash) in /etc/exports
[centos@tsi1539635628271-1 ~]$ sudo vi /etc/exports
[centos@tsi1539635628271-1 ~]$ sudo systemctl restart nfs-server

# on client
[centos@tsi1539166885300-k8s-master ~]$ sudo mkdir /mnt/nfs
[centos@tsi1539166885300-k8s-master ~]$ sudo mount -F 193.62.55.26:/var/nfs /mnt/nfs


[centos@tsi1539166885300-k8s-master ~]$ docker run -i -t --mount type=bind,source=/mnt/nfs,target=/mnt/nfs ikalvari/rfam-cloud
root@858198f42b3e:/# df -h
Filesystem                                                                                          Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-71303370-1907526d5a6b50de3d47f0df811f320cd9b156e67164da6782f2ebb9352b1a9c   10G  4.5G  5.6G  45% /
tmpfs                                                                                                64M     0   64M   0% /dev
tmpfs                                                                                               3.9G     0  3.9G   0% /sys/fs/cgroup
193.62.55.26:/var/nfs                                                                               100G   32M  100G   1% /mnt/nfs
/dev/vda1                                                                                            60G   13G   48G  21% /etc/hosts
shm                                                                                                  64M     0   64M   0% /dev/shm
tmpfs                                                                                               3.9G     0  3.9G   0% /proc/scsi
tmpfs                                                                                               3.9G     0  3.9G   0% /sys/firmware
root@858198f42b3e:/# ls -l /mnt/nfs
total 0
-rw-r--r--. 1 nobody nogroup 0 Oct 16 12:26 david.from.k8s-master
-rw-r--r--. 1 root   root    0 Oct 15 20:40 david.was.here
root@858198f42b3e:/# touch /mnt/nfs/david.from.rfam
root@858198f42b3e:/# ls -l /mnt/nfs
total 0
-rw-r--r--. 1 nobody nogroup 0 Oct 16 12:26 david.from.k8s-master
-rw-r--r--. 1 nobody nogroup 0 Oct 16 12:51 david.from.rfam
-rw-r--r--. 1 root   root    0 Oct 15 20:40 david.was.here
root@858198f42b3e:/#
```

## 2018/10/10

```
[centos@tsi1539166885300-k8s-master ~]$ docker pull ikalvari/rfam-cloud:rfam-fbp
rfam-fbp: Pulling from ikalvari/rfam-cloud
0bd44ff9c2cf: Pull complete 
4421bdb9dd44: Pull complete 
6c247ef38410: Pull complete 
63927961efa3: Pull complete 
3ccaf7e29091: Pull complete 
423784db598f: Pull complete 
f26e156556e0: Pull complete 
e626916861be: Pull complete 
09ac22efa89d: Pull complete 
c2220edd9340: Pull complete 
3cc93deb13c3: Pull complete 
73ee4c0e4801: Pull complete 
56722a3bfbc4: Pull complete 
899c1c629829: Pull complete 
e5d572bdc957: Pull complete 
770d736c00eb: Pull complete 
844b654b3259: Pull complete 
887430298d92: Pull complete 
362d42498d2f: Pull complete 
68128fa8bb40: Pull complete 
af9b845dea85: Pull complete 
aefb2a979a73: Pull complete 
107815bfe45d: Pull complete 
cfc8370777fd: Pull complete 
284ac7559ca0: Pull complete 
7a1e2b416ffd: Pull complete 
e096a567e959: Pull complete 
Digest: sha256:9068c05992df16db7d540e2cd5500249922fba24fc190c679f5a720158b6bec2
Status: Downloaded newer image for ikalvari/rfam-cloud:rfam-fbp

[centos@tsi1539166885300-k8s-master ~]$ docker run -i -t ikalvari/rfam-cloud
Unable to find image 'ikalvari/rfam-cloud:latest' locally
latest: Pulling from ikalvari/rfam-cloud
0bd44ff9c2cf: Already exists 
03119fae4a1d: Pull complete 
add10bf5c352: Pull complete 
9d9fe628aad8: Pull complete 
5b533bc45a2c: Pull complete 
4f56c2751290: Pull complete 
fa3bf969ff43: Pull complete 
5a24a30a128a: Pull complete 
95259fd9ede8: Pull complete 
7ae92e70933e: Pull complete 
dfe8f13749af: Pull complete 
e2bbb9abacae: Pull complete 
559bfb658c80: Pull complete 
c592a239cd31: Pull complete 
c29aebccdd04: Pull complete 
93d9677f7c48: Pull complete 
ae9aae97f329: Pull complete 
56a1dbdb0039: Pull complete 
20e79dd8a3f1: Pull complete 
d33c31a461ee: Pull complete 
4203aa2993da: Pull complete 
666f54064773: Pull complete 
d61dc3ecd430: Pull complete 
275997545460: Pull complete 
fb4581d03eea: Pull complete 
7e459df15357: Pull complete 
Digest: sha256:79a2c935947528101b5c474e13c0e8d7fb74a13ead9107a41dfcf71c4c3257a0
Status: Downloaded newer image for ikalvari/rfam-cloud:latest
root@2880acf11ab7:/#
```

* 4.5 GB is a little big by docker standard. Where and how big is the pipeline vs. test data? 


```
[centos@tsi1539166885300-k8s-master ~]$ docker images
REPOSITORY                                               TAG                 IMAGE ID            CREATED             SIZE
gcr.io/google_containers/kube-proxy-amd64                v1.9.11             0cb625838041        11 days ago         111MB
gcr.io/google_containers/kube-scheduler-amd64            v1.9.11             15efe37b5241        11 days ago         63.7MB
gcr.io/google_containers/kube-controller-manager-amd64   v1.9.11             770301fe32a3        11 days ago         140MB
gcr.io/google_containers/kube-apiserver-amd64            v1.9.11             16cb51b22f6e        11 days ago         213MB
ikalvari/rfam-cloud                                      latest              086ccbea9f74        2 weeks ago         4.54GB
ikalvari/rfam-cloud                                      rfam-fbp            54e54db8f1c2        2 months ago        4.58GB
gcr.io/google_containers/etcd-amd64                      3.1.11              59d36f27cceb        10 months ago       194MB
weaveworks/weave-npc                                     2.1.3               eafec249fefe        10 months ago       46.5MB
weaveworks/weave-kube                                    2.1.3               e102e3c2cf2e        10 months ago       92.6MB
gcr.io/google_containers/pause-amd64                     3.0                 99e59f495ffa        2 years ago         747kB
[centos@tsi1539166885300-k8s-master ~]$

root@2880acf11ab7:/# du -hd2
5.4M    ./bin
0       ./boot
0       ./dev/shm
0       ./dev/mqueue
0       ./dev/pts
0       ./dev
16K     ./etc/alternatives
88K     ./etc/apt
12K     ./etc/cron.daily
24K     ./etc/default
24K     ./etc/dpkg
20K     ./etc/init.d
44K     ./etc/iproute2
4.0K    ./etc/kernel
12K     ./etc/ld.so.conf.d
8.0K    ./etc/logrotate.d
0       ./etc/opt
68K     ./etc/pam.d
0       ./etc/profile.d
0       ./etc/rc0.d
0       ./etc/rc1.d
0       ./etc/rc2.d
0       ./etc/rc3.d
0       ./etc/rc4.d
0       ./etc/rc5.d
0       ./etc/rc6.d
0       ./etc/rcS.d
40K     ./etc/security
4.0K    ./etc/selinux
12K     ./etc/skel
0       ./etc/systemd
4.0K    ./etc/terminfo
4.0K    ./etc/update-motd.d
0       ./etc/.java
32K     ./etc/R
144K    ./etc/X11
4.0K    ./etc/bash_completion.d
4.0K    ./etc/ca-certificates
0       ./etc/dbus-1
4.0K    ./etc/emacs
68K     ./etc/fonts
4.0K    ./etc/gss
4.0K    ./etc/gtk-2.0
4.0K    ./etc/gtk-3.0
4.0K    ./etc/icedtea-web
152K    ./etc/java-8-openjdk
4.0K    ./etc/ldap
0       ./etc/libpaper.d
4.0K    ./etc/logcheck
0       ./etc/network
8.0K    ./etc/perl
4.0K    ./etc/pulse
0       ./etc/sensors.d
4.0K    ./etc/sgml
548K    ./etc/ssh
440K    ./etc/ssl
4.0K    ./etc/vim
12K     ./etc/xdg
16K     ./etc/xml
12K     ./etc/mysql
2.1M    ./etc
0       ./home
12K     ./lib/init
16K     ./lib/lsb
32K     ./lib/systemd
164K    ./lib/terminfo
8.0K    ./lib/udev
13M     ./lib/x86_64-linux-gnu
13M     ./lib
0       ./lib64
0       ./media
0       ./mnt
0       ./opt
0       ./proc/fs
0       ./proc/bus
0       ./proc/irq
0       ./proc/sys
0       ./proc/tty
0       ./proc/acpi
0       ./proc/scsi
0       ./proc/asound
0       ./proc/driver
0       ./proc/sysvipc
du: cannot read directory './proc/1/map_files': Operation not permitted
0       ./proc/1
du: cannot access './proc/18/task/18/fd/4': No such file or directory
du: cannot access './proc/18/task/18/fdinfo/4': No such file or directory
du: cannot access './proc/18/fd/4': No such file or directory
du: cannot read directory './proc/18/map_files': Operation not permitted
du: cannot access './proc/18/fdinfo/4': No such file or directory
0       ./proc/18
0       ./proc
476M    ./root/.cpan
476M    ./root
0       ./run/lock
0       ./run
4.1M    ./sbin
0       ./srv
0       ./sys/fs
0       ./sys/bus
0       ./sys/dev
0       ./sys/devices
0       ./sys/block
0       ./sys/class
0       ./sys/power
0       ./sys/firmware
0       ./sys/kernel
0       ./sys/module
0       ./sys/hypervisor
0       ./sys
0       ./tmp/hsperfdata_root
32K     ./tmp
44M     ./usr/bin
0       ./usr/games
26M     ./usr/include
670M    ./usr/lib
272M    ./usr/local
2.2M    ./usr/sbin
347M    ./usr/share
0       ./usr/src
1.4G    ./usr
0       ./var/backups
1.9M    ./var/cache
33M     ./var/lib
0       ./var/local
572K    ./var/log
0       ./var/mail
0       ./var/opt
0       ./var/spool
0       ./var/tmp
35M     ./var
2.6G    ./Rfam/software
0       ./Rfam/rfamseq
41M     ./Rfam/Bio-Easel
14M     ./Rfam/rfam-family-pipeline
2.6G    ./Rfam
0       ./workdir
4.5G    .
root@2880acf11ab7:/#
```

*  It is unusual that /etc/hosts is used as a mount point. However, OS does not seem to understand it. It is still treated as hosts file with usual loopback adaptors.

```
root@2880acf11ab7:/# df -h
Filesystem                                                                                          Size  Used Avail Use% Mounted on
/dev/mapper/docker-253:1-71303370-b3527c2b45dc9f815a891380ce0aa28afc9326ef11b202f0f464dfa6228e2212   10G  4.5G  5.6G  45% /
tmpfs                                                                                                64M     0   64M   0% /dev
tmpfs                                                                                               3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/vda1                                                                                            60G   12G   49G  20% /etc/hosts
shm                                                                                                  64M     0   64M   0% /dev/shm
tmpfs                                                                                               3.9G     0  3.9G   0% /proc/scsi
tmpfs                                                                                               3.9G     0  3.9G   0% /sys/firmware
root@2880acf11ab7:/# cd /etc/hosts
bash: cd: /etc/hosts: Not a directory
root@2880acf11ab7:/# cd /etc/hosts
bash: cd: /etc/hosts: Not a directory
root@2880acf11ab7:/# ls -l /etc/hosts
-rw-r--r--. 1 root root 174 Oct 10 15:20 /etc/hosts
root@2880acf11ab7:/# cat /etc/hosts
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.2      2880acf11ab7
```
