# C02XD1G9JGH7:ResOps davidyuan$ source ~/Downloads/TSI-CC-Dev-openrc.sh
# Please enter your OpenStack Password:
# C02XD1G9JGH7:ResOps davidyuan$ openstack flavor list
# +--------------------------------------+---------------------------+-------+------+-----------+-------+-----------+
# | ID                                   | Name                      |   RAM | Disk | Ephemeral | VCPUs | Is Public |
# +--------------------------------------+---------------------------+-------+------+-----------+-------+-----------+
# | 1b5cfe48-58fe-4778-b18a-f1067d80ebfa | s1.gargantuan             | 65536 |  100 |         0 |    16 | True      |
# | 316e0e0b-9473-4a10-ac60-743e186a37d7 | c1.rancher-k8s-controller |  8192 |  100 |         0 |     4 | True      |
# | 43890936-a6d7-41d4-8cb6-0c050466e697 | s1.modest                 |  4096 |   30 |         0 |     2 | True      |
# | 63977f51-c652-4fb6-b139-6f7d6c2aa801 | c1.rancher-k8s-node       | 16384 |  100 |         0 |     8 | True      |
# | 6a36101a-21c7-4b97-ac4d-9343fe784028 | s1.large                  |  8192 |   60 |         0 |     4 | True      |
# | 721112dd-2f33-40eb-8975-7bd34dbabfc8 | s1.small                  |  2048 |   20 |         0 |     2 | True      |
# | 7481a9a7-2014-4f37-9617-4697c6ba726a | s1.nano                   |  1024 |    5 |         0 |     1 | True      |
# | 91ba172b-cb4c-453c-b7fc-56cb79c78968 | s1.capacious              |  6144 |   50 |         0 |     4 | True      |
# | bba3e111-9247-40b7-9e55-9c5a1fa8bcfe | s1.medium                 |  4096 |   40 |         0 |     4 | True      |
# | c511a528-319b-4378-b5ff-1f87ac3288af | s1.massive                | 32768 |  100 |         0 |     8 | True      |
# | e9ca7478-7957-4237-b3d0-d4767e1de65f | s1.tiny                   |  1024 |   10 |         0 |     1 | True      |
# | f3fcc537-c1fc-4108-a174-eb5bf52e7481 | s1.huge                   | 16384 |   80 |         0 |     8 | True      |
# | fa85f5f4-4560-4e1b-af95-21df6f714727 | s1.jumbo                  | 12288 |   70 |         0 |     6 | True      |
# | fe8024d9-6450-4a31-8a22-085f0d001141 | c1.rancher-server         | 16384 |   50 |         0 |     4 | True      |
# +--------------------------------------+---------------------------+-------+------+-----------+-------+-----------+

# your Kubernetes cluster name here
cluster_name = "resops"

# SSH key to use for access to nodes
public_key_path = "~/IdeaProjects/tsi-ccdoc/tsi-cc/ResOps/scripts/kubespray/tsi-cc-id_rsa.pub"

# image to use for bastion, masters, standalone etcd instances, and nodes
image = "Ubuntu18.04LTS"

# user on the node (ex. core on Container Linux, ubuntu on Ubuntu, etc.)
ssh_user = "ubuntu"

# 0|1 bastion nodes
number_of_bastions = 1

# s1.medium on TSI-CC-Dev
flavor_bastion = "bba3e111-9247-40b7-9e55-9c5a1fa8bcfe"

# standalone etcds
number_of_etcd = 0

# s1.medium on TSI-CC-Dev
# flavor_etcd="bba3e111-9247-40b7-9e55-9c5a1fa8bcfe"

# masters
number_of_k8s_masters = 0

number_of_k8s_masters_no_etcd = 0

number_of_k8s_masters_no_floating_ip = 0

number_of_k8s_masters_no_floating_ip_no_etcd = 0

# s1.medium on TSI-CC-Dev
# flavor_k8s_master = "bba3e111-9247-40b7-9e55-9c5a1fa8bcfe"

# worker nodes with floating IPs
number_of_k8s_nodes = 35

# worker nodes without floating IPs
number_of_k8s_nodes_no_floating_ip = 0

# s1.large on TSI-CC-Dev
flavor_k8s_node = "6a36101a-21c7-4b97-ac4d-9343fe784028"

# networking
network_name = "resops"

# ext-net-36
#external_net = "e25c3173-bb5c-4bbc-83a7-f0551099c8cd"
# ext-net-31
external_net = "7421d53d-6467-4f29-9d4f-e96e8c85ecd8"
# ext-net-38
external_net = "ca5cf2be-d55c-4f82-be4e-9c8b8cc5b600"

#subnet_cidr = "<cidr>"

dns_nameservers = ["172.22.2.8", "8.8.8.8", "8.8.4.4"]

# floatingip_pool = "ext-net-36"
# floatingip_pool = "ext-net-31"
floatingip_pool = "ext-net-38"

# Least secure: allowing any IP to make a connection
#bastion_allowed_remote_ips = ["0.0.0.0/0"]

k8s_allowed_remote_ips = ["0.0.0.0/0"]

# GlusterFS
# either 0 or more than one
#number_of_gfs_nodes_no_floating_ip = 0

#gfs_volume_size_in_gb = 150

# Container Linux does not support GlusterFS
#image_gfs = "centos74"

# May be different from other nodes
#ssh_user_gfs = "centos"

# s1.medium on TSI-CC-Dev
#flavor_gfs_node = "bba3e111-9247-40b7-9e55-9c5a1fa8bcfe"
