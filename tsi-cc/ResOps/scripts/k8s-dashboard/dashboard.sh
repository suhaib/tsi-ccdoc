#!/usr/bin/env bash

# copied manifest for admin-user RBAC into admin-user.yml
kubectl apply -f source/static/scripts/k8s-dashboard/admin-user.yml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')

kubectl proxy&