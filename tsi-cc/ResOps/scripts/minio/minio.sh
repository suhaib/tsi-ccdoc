#!/usr/bin/env bash

kubectl apply -f source/static/scripts/minio/pvc.yml
kubectl get pvc
kubectl get pv

kubectl apply -f source/static/scripts/minio/minio.yml